package com.example.config;

import com.example.hazelcast.HazelcastConfiguration;
import com.example.hazelcast.NodeCandidate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.hazelcast.leader.LeaderInitiator;
import org.springframework.integration.leader.Candidate;
import org.springframework.integration.leader.event.LeaderEventPublisher;

/**
 * Created by tomask79 on 17.08.17.
 */
@Configuration
public class ServiceConfig {

    @Autowired
    private HazelcastConfiguration hazelcastConfiguration;

    @Autowired
    private LeaderEventPublisher leaderEventPublisher;

    @Bean
    public Candidate nodeService2Candidate() {
        final NodeCandidate candidate =
                new NodeCandidate("service2", HazelcastConfiguration.ROLE_JOB_MAP);
        return candidate;
    }

    @Bean
    public LeaderInitiator initiator() {
        final LeaderInitiator leaderInitiator =
                new LeaderInitiator(hazelcastConfiguration.hazelcastInstance(), nodeService2Candidate());
        leaderInitiator.setLeaderEventPublisher(leaderEventPublisher);
        return leaderInitiator;
    }
}