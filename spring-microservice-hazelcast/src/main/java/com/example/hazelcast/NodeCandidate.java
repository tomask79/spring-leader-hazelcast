package com.example.hazelcast;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.leader.Context;
import org.springframework.integration.leader.DefaultCandidate;

/**
 * Created by tomask79 on 24.08.17.
 */
public class NodeCandidate extends DefaultCandidate {

    @Autowired
    private HazelcastConfiguration hazelcastConfiguration;

    public NodeCandidate(String nodeId, String role) {
        super(nodeId, role);
    }

    @Override
    public void onGranted(Context ctx) {
        super.onGranted(ctx);
        System.out.println("Leader granted to: "+ctx.toString());
        hazelcastConfiguration.hazelcastEventDrivenMessageProducer().start();
    }

    @Override
    public void onRevoked(Context ctx) {
        super.onRevoked(ctx);
        System.out.println("Leader revoked to: "+ctx.toString());
        hazelcastConfiguration.hazelcastEventDrivenMessageProducer().stop();
    }
}