package com.example.hazelcast;

import com.hazelcast.config.Config;
import com.hazelcast.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.hazelcast.CacheListeningPolicyType;
import org.springframework.integration.hazelcast.inbound.HazelcastEventDrivenMessageProducer;
import org.springframework.integration.leader.event.DefaultLeaderEventPublisher;
import org.springframework.integration.leader.event.LeaderEventPublisher;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

/**
 * Created by tomask79 on 04.08.17.
 */
@Configuration
public class HazelcastConfiguration {

    public static String INPUT_JOB_MAP = "randomInputDataMap";
    public static String ROLE_JOB_MAP = "";

    @Bean
    public HazelcastInstance hazelcastInstance() {
        final Config config = new Config();
        return Hazelcast.newHazelcastInstance(config);
    }

    public IMap<String, String> getDistributedMapForJobInput() {
        return hazelcastInstance().getMap(INPUT_JOB_MAP);
    }

    @Bean
    public MessageChannel inputJobChannel() {
        return new DirectChannel();
    }

    @Bean
    public LeaderEventPublisher leaderEventPublisher() {
        return new DefaultLeaderEventPublisher();
    }

    @Bean
    @ServiceActivator(inputChannel = "inputJobChannel")
    public MessageHandler logger() {
        return new LogAndGiveInitiatorHandler();
    }

    @Bean
    public HazelcastEventDrivenMessageProducer hazelcastEventDrivenMessageProducer() {
        final HazelcastEventDrivenMessageProducer producer =
                new HazelcastEventDrivenMessageProducer(
                        getDistributedMapForJobInput()
                );
        producer.setOutputChannel(inputJobChannel());
        producer.setCacheEventTypes("ADDED,REMOVED,UPDATED,CLEAR_ALL");
        producer.setCacheListeningPolicy(CacheListeningPolicyType.SINGLE);
        producer.setAutoStartup(false);

        return producer;
    }
}
