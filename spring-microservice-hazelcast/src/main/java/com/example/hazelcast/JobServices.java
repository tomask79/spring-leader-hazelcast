package com.example.hazelcast;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.hazelcast.leader.LeaderInitiator;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by tomask79 on 10.08.17.
 */
@Service
public class JobServices {

    @Autowired
    private HazelcastConfiguration hazelcastConfiguration;

    @Autowired
    private LeaderInitiator initiator;

    public void distributeRandomValues(final String microServiceName) {
        final String randomKey = UUID.randomUUID().toString();
        final String randomValue = UUID.randomUUID().toString();
        hazelcastConfiguration.getDistributedMapForJobInput().put(microServiceName+randomKey,
                randomValue);
    }

    public void giveUp() {
        if (initiator.getContext().isLeader()) {
            System.out.println("Giving up on leadership: "+initiator.getContext().toString());
            initiator.getContext().yield();
        }
    }
}
