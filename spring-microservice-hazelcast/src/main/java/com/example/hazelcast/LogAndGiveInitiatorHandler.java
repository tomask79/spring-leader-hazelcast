package com.example.hazelcast;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;

/**
 * Created by tomask79 on 24.08.17.
 */
public class LogAndGiveInitiatorHandler implements MessageHandler{

    @Autowired
    private JobServices jobServices;

    @Override
    public void handleMessage(Message<?> message) throws MessagingException {
        System.out.println(message.toString());
        System.out.println("Waiting for another node to take the work...!");
        jobServices.giveUp();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("........");
    }
}
